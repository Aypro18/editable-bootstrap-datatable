import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss']
})
export class SimpleTableComponent implements OnInit {

  wasEdited = false;
  showAddRow = false;

  // new row data
  newRow: any = {
    code: 0,
    l1: '',
    l2: '',
  };

  // tomporary data
  data = [
    {
      code: 0,
      l1: 'random value',
      l2: 'rando field',
    },
    {
      code: 1,
      l1: 'random value',
      l2: 'rando field',
    },
    {
      code: 2,
      l1: 'random value',
      l2: 'rando field',
    },
    {
      code: 3,
      l1: 'random value',
      l2: 'rando field',
    },

  ];

  // main data
  bcData: any[] = [];

  // select values
  itemsList = [
    { id: 0, name: 'First value' },
    { id: 1, name: 'Second value' },
    { id: 2, name: 'Random value' },
    { id: 3, name: 'Third value' },
  ];

  constructor() { }

  ngOnInit(): void {
    // Clone table and backup into bcData
    this.bcData = JSON.parse(JSON.stringify(this.data));
  }

  // find list item values by code
  getItem(code: any): any {
    let i = this.itemsList.findIndex(i => i.id === code);
    return i >= 0 ? this.itemsList[i] : null;
  }

  // select values changes
  valueChanged(index: any, value: any) {
    this.wasEdited = true;
    this.data[index].code = parseInt(value);
  }

  // input values changes
  inputValueChange(index: any, value: any, field: any) {
    this.wasEdited = true;
    field == 'l1' ? this.data[index].l1 = value : this.data[index].l2 = value;
  }

  // input values changes of new row
  inputNewValueChange(value: any, field: any) {
    field == 'l1' ? this.newRow.l1 = value : this.newRow.l2 = value;
  }

  // select of new row values changes 
  updateNew(value: any) {
    this.newRow.code = parseInt(value);
  }

  // save all changes including added rows
  saveChanges() {
    // save the backup of the new table
    this.bcData = JSON.parse(JSON.stringify(this.data));
    this.wasEdited = false;
    this.showAddRow = false;
  }

  // cancel all changes including new rows
  cancelChanges() {
    // get back our backup table
    this.data = JSON.parse(JSON.stringify(this.bcData));
    this.wasEdited = false;
    this.showAddRow = false;
  }

  // add new row
  addNewRow() {
    if (this.showAddRow) {
      // add new row to tomporary data
      this.data.push(this.newRow);
      // rest new row
      this.newRow = {
        code: 0,
        l1: '',
        l2: '',
      }
    } else {
      // start adding rows
      this.showAddRow = true;
      this.wasEdited = true;
    }
  }

  // Get value By Select Value 
  getValueBySelectValue(selectCode: any, column: any): any {
    switch (selectCode) {
      // Study case by The selected value of the selectBox
      case 0:
        // Study case by The column of concerned row
        return column == 'anatal' ? 256 : (column == 'a' ? 453 : (column == 'b' ? 453 : 585));
        break;
      case 1:
        return column == 'anatal' ? 546 : (column == 'a' ? 857 : (column == 'b' ? 965 : 745));
        break;
      case 2:
        return column == 'anatal' ? 956 : (column == 'a' ? 741 : (column == 'b' ? 985 : 554));
        break;
      case 3:
        return column == 'anatal' ? 356 : (column == 'a' ? 742 : (column == 'b' ? 875 : 222));
        break;

      default:
        return column == 'anatal' ? 100 : (column == 'a' ? 200 : (column == 'b' ? 300 : 400));
        break;
    }
  }

}
